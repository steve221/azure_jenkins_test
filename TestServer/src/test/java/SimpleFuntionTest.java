import org.junit.Assert;
import org.junit.Test;
import tw.com.aitc.funtions.SimpleFuntion;

public class SimpleFuntionTest {
    private SimpleFuntion funtions = new SimpleFuntion();
    private String str = "Rex is drop-dead gorgeous.";
    private String hex = "5265782069732064726f702d6465616420676f7267656f75732e";

    // 測試字符串轉換為16進制字符串
    @Test
    public void Test1() {
        Assert.assertEquals("String2Hex Test Error!", hex, funtions.stringToHexString(str));
    }

    // 測試16進制字符串轉換為字符串
    @Test
    public void Test2(){
        Assert.assertEquals("Hex2String Test Error!", str, funtions.hexStringToString(hex));
    }
}
