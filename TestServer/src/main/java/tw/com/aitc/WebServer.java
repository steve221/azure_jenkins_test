package tw.com.aitc;

import org.apache.camel.main.Main;
import tw.com.aitc.routes.*;


public class WebServer {

	public static void main(String... args) throws Exception {
		Main main = new Main();
		main.setPropertyPlaceholderLocations("classpath:system.properties");
		main.configure().addRoutesBuilder(new ResponseRouteBuilder());

		main.run(args);
	}
}

